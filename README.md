# Pvalue Match

Calculate pvalue match tables for use with SStar

## Installation and setup

1.  Clone the workflow

        git clone https://lance_parsons@bitbucket.org/lance_parsons/abwolf_match_pvalue_simulations.git

2.  Setup [Bioconda](https://bioconda.github.io/)

    Install [Miniconda](https://conda.io/miniconda.html) and setup
    [Bioconda](https://bioconda.github.io/) if you have not already done so.


3.  Install dependencies and `archaic_match`

    Bioconda is used to install the various dependencies. We then install the
    development version of `archaic_match` using `pip`.

        conda env create -n abwolf_match_pvalue_simulations -f environment.yml
        source activate abwolf_match_pvalue_simulations

4.  Prepare input data

5.  Edit `config.yaml`

6.  Run workflow

    This workflow uses [Snakemake](http://snakemake.readthedocs.io/) to manage the
    steps of the workflow.

    To see a dry run of the workflow:

        snakemake -nrp

    To run the workflow locally (not using cluster resources):

        snakemake --use-conda -rp -j $num_threads

    To run the workflow configured for `cetus`/`gen-comp1` use `run_cetus.sh`
    which executes the following:

        snakemake --cluster-config 'cetus_cluster.yaml' \
                --drmaa " --cpus-per-task={cluster.n} --mem={cluster.memory} --qos={cluster.qos}" \
                --use-conda -w 60 -rp -j 250 "$@"

